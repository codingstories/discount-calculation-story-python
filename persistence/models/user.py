class UserModel:

    id: int
    first_name: str
    last_name: str
    email: str
    orders: list
    sign_up_date: str
    last_login_date: str
    referer_id: int
    partner_code: str
    firebase_cloud_messaging_token: str
