from persistence.dao.user_dao import UserDao
from persistence.models.user import UserModel
from persistence.persistence_order import PersistenceOrder
from persistence.persistence_user import PersistenceUser


class DefaultUserDao(UserDao):

    def get_user_by_id(self, id: int):
        user = UserModel.query.get(id)
        self.populate_user_with_orders(user=user)
        return user

    def populate_user_with_orders(self, user: PersistenceUser) -> None:
        orderds = PersistenceOrder().get_orders_price()
        user.set_orders(orderds)
