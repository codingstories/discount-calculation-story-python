import abc
from abc import ABC


class UserDao(ABC):

    @abc.abstractmethod
    def get_user_by_id(self, id: int):
        pass
