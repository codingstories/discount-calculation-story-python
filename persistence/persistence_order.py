from businesslogic.order import Order


class PersistenceOrder(Order):

    __id: int
    __order_price: int
    __total_tax: int


    def set_id(self, id_: int):
        self.__id = id_

    def get_orders_price(self):
        return self.__order_price

    def get_total_tax(self):
        return self.__total_tax

    def set_order_price(self, order_price):
        self.__order_price = order_price

    def set_total_tax(self, total_tax):
        self.__total_tax = total_tax
