import abc
from abc import ABC


class Order(ABC):

    MONEY_VALUE_SCALE: int = 2

    def get_gross_order_price(self):
        return self.get_gross_order_price_raw()

    def get_gross_order_price_raw(self):
        return self.get_total_tax() + self.set_order_price()

    @abc.abstractmethod
    def get_orders_price(self):
        pass

    @abc.abstractmethod
    def get_total_tax(self):
        pass

    @abc.abstractmethod
    def set_order_price(self, order_price):
        pass

    @abc.abstractmethod
    def set_total_tax(self, total_tax):
        pass
