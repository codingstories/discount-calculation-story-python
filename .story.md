---
focus: user_facade.py
---
### Conclusion

So what we have done?

Before, the facade was directly depending one single implementation of persistence and procedural and object styles were mixed. It was difficult to change the persistence to some other layer without impacting user facade. Also the behavior was split across many classes ( e.g. user service and user classes).

Now, dependencies between higher-level modules (Facade) and lower-level modules (persistence objects like Dao) are identified properly and are fully depend on abstractions instead of the implementations. We achieved this by creating interfaces and abstract classes and  separating the responsibilities into separate packages(layers). This enables us to replace the persistence layer with another framework without impacting the user facade.  

Also, we replaced data structures (procedural style) with objects (object driven style) and this made sure that data and logic are colocated.
