# Dependency Inversion Story

**To read the story**: https://codingstories.io/stories/6139f4a2f2cd3d0031cd8f91/6193052f7e944b001d796acb

**To code yourself**: https://gitlab.com/coding-stories1/discount-calculation-story-python

**Estimated reading time**: 45 minutes

## Story Outline
Read a small story about calculation of a discount during the online purchase.

This story is about violations of clean design principles with focus on
Dependency Inversion Principle (D in SOLID).

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #clean_design, #python, #dependency_inversion_principle
